package com.example.poznaj_kraje_swiata;


import android.animation.Animator;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.example.poznaj_kraje_swiata.db.StatisticDAO;
import com.example.poznaj_kraje_swiata.models.Statisctic;

import java.io.IOException;
import java.io.InputStream;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import nl.dionsegijn.konfetti.KonfettiView;
import nl.dionsegijn.konfetti.models.Size;
import nl.dionsegijn.konfetti.models.Shape;

/**
 * A simple {@link Fragment} subclass.
 */
public class BlankFragment extends Fragment {
    public ImageButton backButton;
    private TextView questionInformation;
    private ImageView flagsImageView;
    private TextView question;
    private LinearLayout[] guessLinearLayouts;
    private TextView answerText;
    private Set<String> regionSet;
    private List<String> fileNameList;
    private List<String> quizCountryList;
    private SecureRandom random;
    private static final int FLAGS_IN_QUIZ = 10;
    private int guessRows;
    private String correctAnswer;
    private int correctAnswers;
    private Handler handler;
    private int totalGuesses;
    private Animation slideUpAnimation;
    private Animation slideDownAnimation;
    private LinearLayout quizLinearLayout;
    private Statisctic statisctic;
    private long start ;
    private long finish;
    private StatisticDAO statisticDAO;
    public BlankFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_blank,container,false);
        backButton = Main2Activity.getBackButton();
        backButton.setVisibility(View.VISIBLE);
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_blank, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.frameLayout, new Main2ActivityFragment());
                ft.commit();

            }
        });
        statisticDAO = new StatisticDAO(getContext());
        start = System.nanoTime();
        question = view.findViewById(R.id.question);
        questionInformation = view.findViewById(R.id.questionInformation);
        flagsImageView = view.findViewById(R.id.flagsImageView);
        answerText = view.findViewById(R.id.answerText);
        fileNameList = new ArrayList<>();
        quizCountryList = new ArrayList<>();
        random = new SecureRandom();
        correctAnswers = 0;
        totalGuesses = 0;
        handler = new Handler();
        slideDownAnimation = AnimationUtils.loadAnimation(getActivity(),R.anim.slide_down);
        slideUpAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up);
        quizLinearLayout = view.findViewById(R.id.quizLinearLayout);
        guessLinearLayouts = new LinearLayout[4];
        guessLinearLayouts[0] = view.findViewById(R.id.row1LinearLayout);
        guessLinearLayouts[1] = view.findViewById(R.id.row2LinearLayout);
        guessLinearLayouts[2] = view.findViewById(R.id.row3LinearLayout);
        guessLinearLayouts[3] = view.findViewById(R.id.row4LinearLayout);
        List<Statisctic> listStatistic = statisticDAO.getAllNotes();
        for(LinearLayout row: guessLinearLayouts){
            for(int column = 0; column < row.getChildCount(); ++column){
                Button button = (Button) row.getChildAt(column);
                button.setOnClickListener(guessLinearLayoutsListener);
            }
        }
        disableButtons();

    }

    @Override
    public void onStart() {
        super.onStart();
        BlankFragment quizFragment = (BlankFragment) getActivity().getSupportFragmentManager()
                .findFragmentById(R.id.frameLayout);
        quizFragment.updateRegions(PreferenceManager.getDefaultSharedPreferences(getContext()));
        getDataFromAsstes();
        quizFragment.updateRows(PreferenceManager.getDefaultSharedPreferences(getContext()));
        quizFragment.loadQuizCountryList();
        loadNextFlag();



    }

    private View.OnClickListener guessLinearLayoutsListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ++totalGuesses;
            Button button = (Button) v;
            String answer = button.getText().toString();
            String correct_answer = getCountryName(correctAnswer);

            if(answer.equals(correct_answer)){

                ++correctAnswers;
                answerText.setText(answer+"!");
                answerText.setTextColor(getResources().getColor(R.color.correctAnswer, getContext().getTheme()));
                button.setBackgroundColor(getResources().getColor(R.color.correctAnswerButtonBackground));
                disableButtons();
                if(correctAnswers == FLAGS_IN_QUIZ){
                    finish = System.nanoTime();
                    long timeElapsed = (finish - start);
                    String timeGameInMinuts = String.valueOf(TimeUnit.NANOSECONDS.toMinutes(timeElapsed)) + " min";
                    if(timeGameInMinuts.equals("0 min")){
                        timeGameInMinuts = String.valueOf(String.valueOf(TimeUnit.NANOSECONDS.toSeconds(timeElapsed))) + " sek";
                    }
                    Date date = new Date();
                    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                    String dateString = formatter.format(date).toString();
                    statisctic = new Statisctic();
                    statisctic.setDate(dateString);
                    statisctic.setResult(String.valueOf((1000)/(double) totalGuesses));
                    statisctic.setTime_game(timeGameInMinuts);
                    statisctic.setTotalAnswer(String.valueOf(totalGuesses));
                    KonfettiView konfettiView = getActivity().findViewById(R.id.konfettiView);
                    konfettiView.build()
                            .addColors(Color.YELLOW, Color.GREEN, Color.MAGENTA)
                            .setDirection(0.0, 359.0)
                            .setSpeed(1f, 5f)
                            .setFadeOutEnabled(true)
                            .setTimeToLive(2000L)
                            .addShapes(Shape.RECT, Shape.CIRCLE)
                            .addSizes(new Size(12, 5))
                            .setPosition(-50f, konfettiView.getWidth() + 50f, -50f, -50f)
                            .stream(300, 5000L);

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Twój wynik");
                    builder.setMessage(getString(R.string.results, totalGuesses, (1000)/(double) totalGuesses));
                    builder.setPositiveButton(R.string.reset_quiz, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            resetQuiz();
                        }
                    });
                    builder.setCancelable(false);
                    builder.show();
                    statisticDAO.insertStatistic(statisctic);
                    List<Statisctic> listStatistic = statisticDAO.getAllNotes();
                    System.out.print("Maciek");
                }else{

                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            anime();
                        }
                    },2000);


                }
            }else{
                answerText.setText("BŁĄD !!!"+"!");
                answerText.setTextColor(getResources().getColor(R.color.incorrectAnswer, getContext().getTheme()));
                button.setBackgroundColor(getResources().getColor(R.color.incorrectAnswerButtonBackground));
                button.setEnabled(false);
            }

        }
    };


    public void updateRegions(SharedPreferences sharedPreferences){
        /*Pobranie informacji na temat wybranych przez użytkownika obszarów */
        regionSet = sharedPreferences.getStringSet(Main2Activity.REGIONS, null);
    }


    public void getDataFromAsstes(){
        AssetManager assetManager = getActivity().getAssets();
        try{
        for(String region : regionSet){
            String[] countriesFromRegion = assetManager.list(region);
            for(String path : countriesFromRegion){
                fileNameList.add(path.replace(".png", ""));
            }
        }


        }catch(IOException e){
            Log.e("Activity Main","Błąd podczas ładowania Assets",e );
        }
    }


    public void loadQuizCountryList(){
        int flagCounter = 1;
        int numberOfFlags = fileNameList.size();

        while(flagCounter <= FLAGS_IN_QUIZ){
            /* Wybierz losową wartość z zakresu od "0" do liczby flag biorących udiał w quizie*/
            int randomIndex = random.nextInt(numberOfFlags);

            /*Pobierz nazwę pliku o wylosowanym undeksie */
            String fileName = fileName = fileNameList.get(randomIndex);
            /*Jeżeli plik o tej nazwie nie został jeszcze dodany wylosowany, to dodaj go do listy */
            if(!quizCountryList.contains(fileName)){
                quizCountryList.add(fileName);
                ++flagCounter;
            }
        }

    }

    private void disableButtons(){
        for(int row = 0 ; row < guessRows; ++row){
            LinearLayout guessRow = guessLinearLayouts[row];
            for(int column = 0; column < 2; column++){
                guessRow.getChildAt(column).setEnabled(false);
            }
        }
    }


    public void updateRows(SharedPreferences sharedPreferences){
        String choices = sharedPreferences.getString(Main2Activity.CHOICES, null);
        guessRows = Integer.parseInt(choices)/2;

        for(LinearLayout layout: guessLinearLayouts){
            layout.setVisibility(View.GONE);
        }

        for(int row = 0; row < guessRows; row ++){
            guessLinearLayouts[row].setVisibility(View.VISIBLE);

        }
    }

    private void loadNextFlag(){
        String nextImage = quizCountryList.remove(0);
        correctAnswer = nextImage;
        questionInformation.setText(getString(R.string.detail_question, (correctAnswers+1), FLAGS_IN_QUIZ));
        String region = nextImage.substring(0,nextImage.indexOf("-"));
        answerText.setText("");
        AssetManager asset = getActivity().getAssets();

       /* if(correctAnswers > 0){
            flagsImageView.setAnimation(slideUpAnimation);
        }*/

        try{
            InputStream inputStream = asset.open(region + "/" + nextImage + ".png");
            Drawable drawable = Drawable.createFromStream(inputStream, nextImage);
            flagsImageView.setImageDrawable(drawable);
          //  flagsImageView.setAnimation(slideDownAnimation);
        }catch (IOException e){
            Log.e("Fragment Blank", "Błąd podczas ładowania "+ nextImage, e);
        }
        Collections.shuffle(fileNameList);

        /*Umieszczenie prawidłowej odpowiedzi na końcu listy*/
        int correct = fileNameList.indexOf(correctAnswer);
        fileNameList.add(fileNameList.remove(correct));

        /*Dodanie tekstu do przycisków odpowiedzi*/
        for(int row = 0; row< guessRows; ++row){
            for(int column =0; column<2; column++){
                /*Uzyskanie dostępu do przycisku i zmienienie jego stanu na enabled*/
                Button guessButton = (Button)guessLinearLayouts[row].getChildAt(column);
                guessButton.setBackgroundColor(getResources().getColor(R.color.appBarIconBackground));
                guessButton.setEnabled(true);

                /*Pobierz nazwę kraju i ustaw ją w widoku Button*/
                String fileName = fileNameList.get((row*2)+column);
                guessButton.setText(getCountryName(fileName));
            }
        }
        /*Dodanie poprawnej odpowiedzi do losowo wybranego rzycisku  */
        int row = random.nextInt(guessRows);
        int column = random.nextInt(2);
        LinearLayout randomRow = guessLinearLayouts[row];
        String countryName = getCountryName(correctAnswer);
        ((Button) randomRow.getChildAt(column)).setText(countryName);

    }

    private String getCountryName(String name){
        return name.substring(name.indexOf("-") + 1).replace("_", " ");
    }


    private void slideDownAnimation(int time){
        slideDownAnimation.setDuration(time);
        flagsImageView.setAnimation(slideDownAnimation);

    }

    private void slideUpAnimation(int time){
        slideUpAnimation.setDuration(time);
        flagsImageView.setAnimation(slideUpAnimation);

    }

    private void anime(){
        if(correctAnswers == 0) return;

        /*Obliczanie współrzędnych środka rozkładu */
        int centerX = (quizLinearLayout.getLeft()+ quizLinearLayout.getRight())/2;
        int centerY = (quizLinearLayout.getTop()+quizLinearLayout.getBottom())/2;

        /*Obliczenie promienia animacji okregu */
        int radius = Math.max(quizLinearLayout.getWidth(), quizLinearLayout.getHeight());

        /*Zdefiniowanie obiketu animacji*/
        Animator animator ;
        if(correctAnswers > 0){
            /*Utworzenie animacji */
            animator = ViewAnimationUtils.createCircularReveal(quizLinearLayout, centerX, centerY,radius,0);
            animator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    loadNextFlag();
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
        }else{
            /*Wariant animacji odkrywającej flagę */
            animator = ViewAnimationUtils.createCircularReveal(quizLinearLayout,centerX,centerY, 0,radius);

        }
        /*określenie czasu trwania animacji*/
        animator.setDuration(500);
        /*Uruchomienie animacji*/
        animator.start();

    }


    public void resetQuiz(){
        /*Uzyskaj dostęp do folderu assets */
        AssetManager assets = getActivity().getAssets();

        /*Wyczyść listę z nazwami flag */
        fileNameList.clear();

        /*Pobierz nazwy plików obrazów flag z wybranych przez użytkownika obszarów */
        try{
            /* Pętla przechodząca przez każdy obszar - czyli przez każdy folder w folderze assets*/
            for(String region: regionSet){
                /*Pobranie nazw wszystkich plików znajdujących się w obszarze danego obszaru*/
                String[] paths = assets.list(region);
                /*Usunięcie z nazw plików ich rozszenia formatu */
                for(String path : paths){
                    fileNameList.add(path.replace(".png", ""));
                }
            }
        }catch (IOException ex){
            Log.e("BLANK FRAGMENT", "Błąd podczas ładowania plików z obrazami flag", ex);
        }

        /*Zresetowanie poprawnych i wszystkich odpowieddzi */
        correctAnswers = 0;
        totalGuesses = 0;

        /*Wyczyść listy krajów */
        quizCountryList.clear();

        /*Inicjalizacja zmiennych wukorzystywanych przy losowanu flag*/
        int flagCounter = 1;
        int numberOfFlags = fileNameList.size();

        /*Losowanie flag*/
        while(flagCounter <= FLAGS_IN_QUIZ){
            /* Wybierz losową wartość z zakresu od "0" do liczby flag biorących udiał w quizie*/
            int randomIndex = random.nextInt(numberOfFlags);

            /*Pobierz nazwę pliku o wylosowanym undeksie */
            String fileName = fileName = fileNameList.get(randomIndex);
            /*Jeżeli plik o tej nazwie nie został jeszcze dodany wylosowany, to dodaj go do listy */
            if(!quizCountryList.contains(fileName)){
                quizCountryList.add(fileName);
                ++flagCounter;
            }
        }
        /*Załaduj flagę */
        BlankFragment quizFragment = (BlankFragment) getActivity().getSupportFragmentManager()
                .findFragmentById(R.id.frameLayout);
        quizFragment.updateRegions(PreferenceManager.getDefaultSharedPreferences(getContext()));
        getDataFromAsstes();
        quizFragment.updateRows(PreferenceManager.getDefaultSharedPreferences(getContext()));
        quizFragment.loadQuizCountryList();
        loadNextFlag();
    }




}
