package com.example.poznaj_kraje_swiata;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.resources.TextAppearance;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.poznaj_kraje_swiata.db.StatisticDAO;
import com.example.poznaj_kraje_swiata.db.tables.Statistics;
import com.example.poznaj_kraje_swiata.models.Statisctic;

import java.util.Formatter;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class StatisticsFragment extends Fragment {

    private ImageButton backButton;
    private StatisticDAO statisticDAO;
    private TableLayout tableLayout;
    private int countListStatistics;
    private Button deleteYourResult;

    public StatisticsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_statistics, container, false);
    }

    @Override
    public void onViewCreated( View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tableLayout = view.findViewById(R.id.statisticsTable);
        deleteYourResult = view.findViewById(R.id.deleteYourResult);
        backButton = Main2Activity.getBackButton();

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.frameLayout, new Main2ActivityFragment());
                ft.commit();

            }
        });

        deleteYourResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setCancelable(true);
                builder.setTitle("Usuwanie wyników");
                builder.setMessage(getString(R.string.delete_result_message));
                builder.setPositiveButton(R.string.yes_delete_result, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        statisticDAO.deleteAllRows();
                        FragmentManager fm = getActivity().getSupportFragmentManager();
                        FragmentTransaction ft = fm.beginTransaction();
                        ft.replace(R.id.frameLayout, new StatisticsFragment());
                        ft.commit();
                    }
                });

                builder.setNegativeButton(R.string.no_delete_result, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                });
                builder.show();
            }
        });

        statisticDAO = new StatisticDAO(getContext());
        List<Statisctic> myListStatistic = statisticDAO.getAllNotes();
        countListStatistics = myListStatistic.size();
        TextView[] tabTextView;
        TableRow row;
        if(!myListStatistic.isEmpty()){
            for (Statisctic item : myListStatistic){
                tabTextView = createTextViewToRow(4, item);
                row = createRowToStatisticTable(tabTextView);
                tableLayout.addView(row);
            }

        }else{

            Log.e("PUSTA LISTA", "Pusta lista resultatu z bazy");

        }

    }



    private TextView[] createTextViewToRow(int size, Statisctic statisctic){
        TextView[] tabTextView = new TextView[4];

        for(int i = 0; i < size; ++i){
            tabTextView[i] = new TextView(getContext());
            tabTextView[i].setGravity(Gravity.CENTER);
            tabTextView[i].setPadding(3,3,3,3);
            tabTextView[i].setTextAppearance(getContext(), android.R.style.TextAppearance_DeviceDefault);
            tabTextView[i].setTextColor(getResources().getColor(R.color.textColor));
        }
        double result = Double.valueOf(statisctic.getResult());
        Formatter fmt = new Formatter();
        String tmp = fmt.format("%.2f", result).toString();
        tabTextView[0].setText(statisctic.getDate());
        tabTextView[1].setText(statisctic.getTotalAnswer());
        tabTextView[2].setText(statisctic.getTime_game());
        tabTextView[3].setText(tmp);


        return tabTextView;
    }

    private TableRow createRowToStatisticTable(TextView[] tab){
        TableRow row = new TableRow(getContext());

        if(tab.length > 0){
            for(int i = 0; i < tab.length; ++i){
                row.addView(tab[i]);
            }
        }else{
            Exception e = new Exception();

        }

        return row;
    }
}
