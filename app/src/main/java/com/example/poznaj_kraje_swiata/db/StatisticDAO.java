package com.example.poznaj_kraje_swiata.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.icu.util.LocaleData;
import android.provider.ContactsContract;

import com.example.poznaj_kraje_swiata.db.tables.Statistics;
import com.example.poznaj_kraje_swiata.models.Statisctic;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StatisticDAO {

    private DBHelper dbHelper;

    public StatisticDAO(Context context){
        dbHelper = new DBHelper(context);
    }

    public void insertStatistic(final Statisctic statistic) {
        String[] tab_tmp = new String[4];
        tab_tmp[0] = statistic.getDate();
        tab_tmp[1] = statistic.getResult();
        tab_tmp[2] = statistic.getTime_game();
        tab_tmp[3] = statistic.getTotalAnswer();


        ContentValues contentValues = new ContentValues();
        contentValues.put(Statistics.Columns.STATISTIC_DATE, statistic.getDate());
        contentValues.put(Statistics.Columns.STATISTIC_RESULT, statistic.getResult());
        contentValues.put(Statistics.Columns.STATISTIC_TIME_GAME, statistic.getTime_game());
        contentValues.put(Statistics.Columns.STATISTIC_TOTAL_ANSWER, statistic.getTotalAnswer());

        dbHelper.getWritableDatabase().insert(Statistics.TABLE_NAME, null, contentValues);
    }


    public Statisctic getNoteById(final int id) {
        Cursor cursor = dbHelper.getReadableDatabase().rawQuery("select * from " + Statistics.TABLE_NAME
                + " where " + Statistics.Columns.STATISTIC_ID + " = " + id, null);
        if (cursor.getCount() == 1) {
            cursor.moveToFirst();
            return mapCursorToNote(cursor);
        }
        return null;
    }

    // aktualizacja notatki w bazie
    public void updateNote(final Statisctic note) {
        ContentValues contentValues = new ContentValues();

    }

    // usunięcie notatki z bazy
    public void deleteNoteById(final Integer id) {
        dbHelper.getWritableDatabase().delete(Statistics.TABLE_NAME,
                " " + Statistics.Columns.STATISTIC_ID + " = ? ",
                new String[]{id.toString()}
        );
    }

    // pobranie wszystkich notatek
    public List getAllNotes() {
        Cursor cursor = dbHelper.getReadableDatabase().query(Statistics.TABLE_NAME,
                new String[]{Statistics.Columns.STATISTIC_ID,
                        Statistics.Columns.STATISTIC_DATE,
                        Statistics.Columns.STATISTIC_RESULT,
                        Statistics.Columns.STATISTIC_TIME_GAME,
                        Statistics.Columns.STATISTIC_TOTAL_ANSWER},

                null, null, null, null, null
        );

        List results = new ArrayList<>();

        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                results.add(mapCursorToNote(cursor));
            }
        }

        return results;
    }

    // zamiana cursora na obiekt notatki
    private Statisctic mapCursorToNote(final Cursor cursor) {


        Map<String, Integer> indexColumn = new HashMap<String, Integer>();
        indexColumn.put("ID", cursor.getColumnIndex(Statistics.Columns.STATISTIC_ID));
        indexColumn.put("DATE", cursor.getColumnIndex(Statistics.Columns.STATISTIC_DATE));
        indexColumn.put("RESULT", cursor.getColumnIndex(Statistics.Columns.STATISTIC_RESULT));
        indexColumn.put("TIME_GAME", cursor.getColumnIndex(Statistics.Columns.STATISTIC_TIME_GAME));
        indexColumn.put("TOTAL_ANSWER", cursor.getColumnIndex(Statistics.Columns.STATISTIC_TOTAL_ANSWER));

        Statisctic statisctic = new Statisctic();
        statisctic.setId(cursor.getInt(indexColumn.get("ID")));
        statisctic.setDate(cursor.getString(indexColumn.get("DATE")));
        statisctic.setResult(cursor.getString(indexColumn.get("RESULT")));
        statisctic.setTime_game(cursor.getString(indexColumn.get("TIME_GAME")));
        statisctic.setTotalAnswer(cursor.getString(indexColumn.get("TOTAL_ANSWER")));


        return statisctic;
    }

    public void deleteAllRows(){
        dbHelper.getWritableDatabase().delete(Statistics.TABLE_NAME,null,null);

    }

}
