package com.example.poznaj_kraje_swiata.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.poznaj_kraje_swiata.db.tables.Statistics;

public class DBHelper extends SQLiteOpenHelper {

    private final static int DB_VERSION = 1;
    private final static String DB_NAME = "StatisticsDB.db";

    public DBHelper(Context context){
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table "
                        + Statistics.TABLE_NAME
                        + " ( "
                        + Statistics.Columns.STATISTIC_ID
                        + " integer primary key, "
                        + Statistics.Columns.STATISTIC_DATE
                        + " text, "
                        + Statistics.Columns.STATISTIC_RESULT
                        + " text, "
                        + Statistics.Columns.STATISTIC_TIME_GAME
                        + " text, "
                        + Statistics.Columns.STATISTIC_TOTAL_ANSWER
                        + " text )"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Statistics.TABLE_NAME);
        onCreate(db);
    }
}
