package com.example.poznaj_kraje_swiata.db.tables;

public interface Statistics {
    String TABLE_NAME = "Statistics";

    interface Columns {
        String STATISTIC_ID = "_id";
        String STATISTIC_DATE = "date";
        String STATISTIC_RESULT = "result";
        String STATISTIC_TIME_GAME = "time_game";
        String STATISTIC_TOTAL_ANSWER = "totalAnswer";
    }
}
