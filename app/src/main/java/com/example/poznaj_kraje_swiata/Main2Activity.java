package com.example.poznaj_kraje_swiata;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.poznaj_kraje_swiata.db.StatisticDAO;

import java.util.Set;

public class Main2Activity extends AppCompatActivity {

    private Button startButton;
    private static ImageButton backButton;
    private Set<String> regions;
    public static final String CHOICES = "pref_numberOfChoices";
    public static final String REGIONS = "pref_regionsToInclude";
    private boolean preferencesChanged = true;
    //private StatisticDAO statisticDAO;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        backButton = findViewById(R.id.backButton);
        backButton.setVisibility(View.GONE);
        //statisticDAO = new StatisticDAO(this);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.frameLayout, new Main2ActivityFragment());
        ft.commit();
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        PreferenceManager.getDefaultSharedPreferences(Main2Activity.this)
                .registerOnSharedPreferenceChangeListener(preferencesChangedListener);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(preferencesChanged){
            Main2ActivityFragment quizFragment = (Main2ActivityFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.frameLayout);
           // BlankFragment.updateRows(PreferenceManager.getDefaultSharedPreferences(this));
           // blankFragment.updateRegions(PreferenceManager.getDefaultSharedPreferences(this));
           // blankFragment.resetQuiz();
            preferencesChanged = false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public static ImageButton getBackButton(){
        return backButton;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()== R.id.action_settings){
            Intent preferencesIntent = new Intent(this, SettingsActivity.class);
            startActivity(preferencesIntent);
        }

        if(item.getItemId()==R.id.action_statistic){
            backButton.setVisibility(View.VISIBLE);
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.frameLayout, new StatisticsFragment());
            ft.commit();
        }

        return super.onOptionsItemSelected(item);
    }

    private SharedPreferences.OnSharedPreferenceChangeListener preferencesChangedListener =
            new SharedPreferences.OnSharedPreferenceChangeListener(){
                @Override
                public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                    preferencesChanged = true;

                    /*Instrukcja warunkowa dla rodzaju zmienionych ustawień */
                    if(key.equals(CHOICES)){


                    }else if(key.equals(REGIONS)){
                        /*Pobranie listy wybranyh obszarów*/
                        Set<String> regions = sharedPreferences.getStringSet(REGIONS, null);

                        /*Jeżeli wybrano więcej niż jeden obszar*/
                        if(regions != null && regions.size() > 0){
                            //updateRegions(sharedPreferences);
                            //resetQuiz();
                            Toast.makeText(Main2Activity.this, R.string.message_default_message, Toast.LENGTH_SHORT).show();
                        }else{
                            /*Edycja preferencji może odbyć się tylko przy użyciu obiketu Editor. */
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            regions.add(getString(R.string.default_region));
                            editor.putStringSet(REGIONS, regions);
                            editor.apply();
                            Toast.makeText(Main2Activity.this, R.string.default_region_message, Toast.LENGTH_LONG).show();
                        }
                        /*Informawanie użytkownika o restarcie quizu*/

                    }

                }
            };


}
