package com.example.poznaj_kraje_swiata.models;

import android.icu.util.LocaleData;

import java.sql.Time;
import java.util.Date;

public class Statisctic {

    private Integer id;
    private String date;
    private String result;
    private String time_game;
    private String totalAnswer;

    public String getDate() {
        return date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getTime_game() {
        return time_game;
    }

    public void setTime_game(String time_game) {
        this.time_game = time_game;
    }

    public String getTotalAnswer() {
        return totalAnswer;
    }

    public void setTotalAnswer(String totalAnswer) {
        this.totalAnswer = totalAnswer;
    }
}
